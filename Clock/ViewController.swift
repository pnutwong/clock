//
//  ViewController.swift
//  Clock
//
//  Created by GDD Student on 28/4/16.
//  Copyright © 2016 pnut production. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var timeLabel: UILabel!
    
    let clock = Clock()
    
    func updateTimeLabel()
    {
        let formatter = NSDateFormatter()
        formatter.timeStyle = .ShortStyle
        timeLabel.text = formatter.stringFromDate(clock.currentTime)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        NSNotificationCenter.defaultCenter().addObserver(self,
            selector: "updateTimeLabel",
            name:UIApplicationWillEnterForegroundNotification,
            object: nil)
        print("viewDidLoad")
        
    }
    
    override func viewWillAppear(animated: Bool)
    {
        super.viewWillAppear(animated)
        updateTimeLabel()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    deinit
    {
        NSNotificationCenter.defaultCenter().removeObserver(self) 
    }

}

